import * as React from "react";
import styled from "styled-components";

const Container = styled.div`
  width: 40%;
  margin: 0px auto;
  height: 300px;
  align-items: center;
  display: flex;
  text-align: center;
  background-color: lightblue;
`;

const TextContent = styled.div`
  width: 100%;
  text-align: center;
  cursor: pointer;
  justify-content: center;
  display: flex;
  align-items: center;
`;

const DragAndDropDown = ({ onSelect = () => {} }) => {
  const [selectedFiles, setSelectedFiles] = React.useState([]);

  const onSelectFiles = (e) => {
    setSelectedFiles(e.target.files);
    onSelect(e.target.files);
  };

  const dragOver = (e) => {
    e.preventDefault();
  };

  const dragEnter = (e) => {
    e.preventDefault();
  };

  const dragLeave = (e) => {
    e.preventDefault();
  };

  const fileDrop = (e) => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    setSelectedFiles(files);
    onSelect(files);
  };

  return (
    <Container
      onDragOver={dragOver}
      onDragEnter={dragEnter}
      onDragLeave={dragLeave}
      onDrop={fileDrop}
    >
      <TextContent>
        {selectedFiles.length === 0
          ? "Drag and Drop"
          : `${selectedFiles.length} image${
              selectedFiles.length !== 1 ? "s" : ""
            } selected`}
        <input
          type="file"
          style={{ opacity: "0", position: "absolute" }}
          accept="image/*"
          multiple
          onChange={onSelectFiles}
          data-testid="input"
        />
      </TextContent>
    </Container>
  );
};

export default DragAndDropDown;
