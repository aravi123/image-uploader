import { render, cleanup, fireEvent } from "@testing-library/react";

import DragAndDropDown from "../DragAndDropDown";

afterEach(cleanup);

describe("DragAndDropDown", () => {
  it("matches snapshot", () => {
    const wrapper = render(<DragAndDropDown />);
    expect(wrapper).toMatchSnapshot();
  });
  it("renders without crashing", () => {
    const wrapper = render(<DragAndDropDown />);
    expect(wrapper.getByText("Drag and Drop")).toBeDefined();
  });
  it("On drag of files update the state of the selected files", () => {
    const handleSelect = jest.fn();
    const wrapper = render(<DragAndDropDown onSelect={handleSelect} />);

    fireEvent.drop(wrapper.getByText("Drag and Drop"), {
      dataTransfer: {
        files: [new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" })],
      },
    });
    expect(wrapper.getByText(/1 image selected/)).toBeDefined();
    expect(handleSelect).toHaveBeenCalledTimes(1);
  });
  it("On click of input open a file popup", () => {
    const handleSelect = jest.fn();
    const wrapper = render(<DragAndDropDown onSelect={handleSelect} />);

    fireEvent.change(wrapper.getByTestId("input"), {
      target: {
        files: [new File(["(⌐□_□)"], "chucknorris.png", { type: "image/png" })],
      },
    });
    expect(wrapper.getByText(/1 image selected/)).toBeDefined();
    expect(handleSelect).toHaveBeenCalledTimes(1);
  });
});
