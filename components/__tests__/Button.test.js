import { render, cleanup, fireEvent } from "@testing-library/react";

import Button from "../Button";

afterEach(cleanup);

describe("Button", () => {
  it("matches snapshot", () => {
    const wrapper = render(<Button>Hello</Button>);
    expect(wrapper).toMatchSnapshot();
  });
  it("renders without crashing", () => {
    const wrapper = render(<Button>Hello</Button>);
    expect(wrapper.getByText("Hello")).toBeDefined();
  });
  it("onClick to be called on click", () => {
    const handleClick = jest.fn();
    const wrapper = render(<Button onClick={handleClick}>Hello</Button>);
    fireEvent.click(wrapper.getByText("Hello"));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
