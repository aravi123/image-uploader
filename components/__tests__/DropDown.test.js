import { render, cleanup, fireEvent } from "@testing-library/react";

import DropDown from "../DropDown";

afterEach(cleanup);

describe("DropDown", () => {
  it("matches snapshot", () => {
    const wrapper = render(
      <DropDown
        list={[
          {
            key: "50",
            value: "50X50",
          },
        ]}
        placeholder="DropDown"
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
  it("Renders without crashing", () => {
    const wrapper = render(
      <DropDown
        list={[
          {
            key: "50",
            value: "50X50",
          },
        ]}
        placeholder="DropDown"
      />
    );
    expect(wrapper.getByText("DropDown")).toBeDefined();
  });
  it("on click of dropdown show list", () => {
    const handleSelect = jest.fn();
    const wrapper = render(
      <DropDown
        list={[
          {
            key: "50",
            value: "50X50",
          },
        ]}
        placeholder="DropDown"
        onSelect={handleSelect}
      />
    );
    fireEvent.click(wrapper.getByText("DropDown"));
    expect(wrapper.getByText("50X50")).toBeDefined();

    fireEvent.click(wrapper.getByText("50X50"));
    expect(handleSelect).toHaveBeenCalledTimes(1);
    expect(wrapper.getByText("50X50")).toBeDefined();
  });
});
