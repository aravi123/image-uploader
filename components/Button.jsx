// Libraries
import * as React from "react";
import styled from "styled-components";

// styled components
const ButtonWrapper = styled.div`
  cursor: pointer;
  border-radius: 4px;
  width: 100%;
`;

const ButtonText = styled.button`
  text-align: center;
  color: white;
  border-radius: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #0378ff;
  border: 1px solid #0378ff;
  color: white;
  width: 140px;
  height: 32px;
`;

const Button = ({ children, onClick = () => {}, style }) => {
  return (
    <ButtonWrapper>
      <ButtonText onClick={onClick} style={style}>
        {children}
      </ButtonText>
    </ButtonWrapper>
  );
};

export default Button;
