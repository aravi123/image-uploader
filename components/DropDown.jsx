// Libraries
import * as React from "react";
import styled from "styled-components";

// styled components
const DropDownButtonContainer = styled.div`
  border-radius: 4px;
  background-color: white;
  width: 140px;
  height: 32px;
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  color: white;
  border: 1px solid #0378ff;
  color: #0378ff;
`;

const Container = styled.div`
  width: 140px;
  height: 32px;
`;

const DropDownContainer = styled.div`
  border: 1px solid #0378ff;
  padding: 4px;
  z-index: 99;
  background-color: white;
  position: relative;
`;

const DropDown = ({
  list = [],
  onSelect = () => {},
  style = {},
  placeholder = "",
}) => {
  const [showDropDown, toggleDropDown] = React.useState(false);
  const [selectedData, setSelectedData] = React.useState({});

  const onDropDownClick = () => {
    toggleDropDown(!showDropDown);
  };

  const onSelectDropDown = (data) => {
    setSelectedData(data);
    onSelect(data.key);
    onDropDownClick();
  };

  return (
    <Container style={style}>
      <DropDownButtonContainer onClick={onDropDownClick}>
        {selectedData.value || placeholder}
        <img
          src={showDropDown ? "arrowUp.svg" : "arrowDown.svg"}
          style={{ width: "14px" }}
        />
      </DropDownButtonContainer>
      {showDropDown && (
        <DropDownContainer>
          {list.map(({ key, value }) => (
            <div
              key={key}
              style={{ cursor: "pointer" }}
              onClick={() => {
                onSelectDropDown({ key, value });
              }}
            >
              {value}
            </div>
          ))}
        </DropDownContainer>
      )}
    </Container>
  );
};

export default DropDown;
