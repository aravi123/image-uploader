import React from "react";
import Head from "next/head";
// components
import Button from "../components/Button";
import DropDown from "../components/DropDown";
import DragAndDropDown from "../components/DragAndDropDown";

// utils
import { apiRequest } from "../utils/request";

const Dashboard = () => {
  const [data, setData] = React.useState({});

  const onSelectedData = (key, value) => {
    setData({
      ...data,
      [key]: value,
    });
  };

  const onDoneClick = async () => {
    try {
      const formData = new FormData();
      for (let i = 0; i < data.files.length; i++) {
        formData.append("images", data.files[i], data.files[i].name);
      }
      formData.append("resolution", data.resolution);
      const response = await apiRequest.post("/api/upload", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      // set response data
      setData({
        ...data,
        uploadedImages: response.data.uploadedImages,
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div
      style={{
        width: "70%",
        margin: "30px auto",
      }}
    >
      <Head>
        <title>Image Uploader</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <DragAndDropDown onSelect={(files) => onSelectedData("files", files)} />
      <div>
        <DropDown
          style={{ margin: "20px auto" }}
          placeholder="Select resolution"
          list={[
            { key: "50", value: "50 X 50" },
            { key: "10", value: "10 X 10" },
            { key: "20", value: "20 X 20" },
          ]}
          onSelect={(resolution) => onSelectedData("resolution", resolution)}
        />
        <Button style={{ margin: "20px auto" }} onClick={onDoneClick}>
          Generate Thumbnail
        </Button>
        {data.uploadedImages &&
          data.uploadedImages.map((url) => (
            <>
              <a key={url} href={`uploads/${url}`} target="_blank">
                {url}
              </a>
              <br />
            </>
          ))}
      </div>
    </div>
  );
};

export default Dashboard;
