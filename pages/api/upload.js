const util = require("util");
const multer = require("multer");
const imageThumbnail = require("image-thumbnail");
const fs = require("fs");

let options = { jpegOptions: { force: true, quality: 100 } };

export const config = {
  api: {
    bodyParser: false,
  },
};

const uploadFilesToCDN = async (files, originalFiles) => {
  try {
    let uploadedFiles = [];
    files.forEach((file, index) => {
      uploadedFiles.push(uploadFile(file, originalFiles[index].originalname));
    });
    return await Promise.all(uploadedFiles);
  } catch (err) {
    throw err;
  }
};

const uploadFile = (file, fileName) => {
  // upload to SOME CDN from here.
  fs.writeFileSync(`public/uploads/${fileName}`, file);
  return fileName;
};

const generateThumbnail = async (files, resolution) => {
  try {
    let imageThumbnails = [];
    files.forEach((file) => {
      imageThumbnails.push(
        imageThumbnail(file.buffer, {
          ...options,
          width: parseInt(resolution),
          height: parseInt(resolution),
        })
      );
    });

    return await Promise.all(imageThumbnails);
  } catch (err) {
    throw err;
  }
};

const upload = async (req, res) => {
  try {
    await util.promisify(multer().any())(req, res);

    // wait for the thumbnail to be generated
    const thumbnails = await generateThumbnail(req.files, req.body.resolution);
    const uploadedImages = await uploadFilesToCDN(thumbnails, req.files);

    res.send({
      status: 200,
      uploadedImages,
    });
  } catch (err) {
    console.log(err);
    res.send({
      status: 400,
    });
  }
};

export default upload;
