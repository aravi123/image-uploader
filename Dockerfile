# Dockerfile

# base image
FROM node:alpine
RUN apk add bash make gcc g++ libpng-dev python3 

# create & set working directory
RUN mkdir -p /usr/src
WORKDIR /usr/src

# copy source files
COPY . /usr/src

# install dependencies
RUN npm install

# start app
RUN npm run build
EXPOSE 3000
CMD npm run start